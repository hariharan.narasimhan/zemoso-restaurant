$(function() {

	function populateTables(tablesData, currentTableIndex) {
		tables = tablesData;
		$("#tables").empty();
		let tableIndex = 0;
		tables.forEach( function(table) {
			let tableString = '<div class="card border" >\
			<div class="card-body table-body ' + ((currentTableIndex === tableIndex) ? 'bg-warning' : '') + '" id="table_' + tableIndex+ '" data-toggle="modal" data-target="#tableModalCenter">\
				<b>' + table.tableName + '</b><br/>' + 
				'Rs.' + table.total  + ' | Total items:' + table.items.length + 
			'</div>\
		</div><br/>';
			let uiTable = $(tableString);
			uiTable.droppable({
				drop: function(event, ui) {
					let itemIndex = $(ui.draggable).data("item-index");
					console.log(items);
					modifyTableItem(items[itemIndex], 1, table, true);
					searchRefresh();
				}
			});
			$("#tables").append(uiTable);
			tableIndex++;
		});
	}

	function closePopupAction() {
		let tableId = $("#tableModalCenter").data("table-id");
		console.log('table id', tableId);
		$('#table_' + tableId).removeClass('bg-warning');
	}

	function refreshModal(element, currentTableIndex) {
		let table = tables[currentTableIndex];
		$("#tableModalLongTitle").text(table.tableName + ' | Order Details');
		let modalTable = '<table> \
			<thead><th>S.No</th> <th>Item</th> <th>Price</th> <th></th> <th></th></thead>\
			<tbody>';
		calculateTableTotal(table);
		for (let i = 0; i < table.items.length; i++) {
			let item = table.items[i];
			let itemQuantityId = 't-'+ currentTableIndex + '-iq-' + i;
			let deleteItemId = 't-'+ currentTableIndex + '-id-' + i;
			modalTable += '<tr>' +
				'<td>' + (i + 1) + '</td><td>' + item.itemName + '</td>';
			modalTable += '<td>' + item.price + '</td> \
				<td><label for="quantity" class="control-label">Number Of Servings<label/> \
				<input type="number" id="' + itemQuantityId + '" class="item-quantity" min="0"\
				name="quantity" value="' + item.quantity + '"/></td>\
				<td><i class="fa fa-trash" aria-hidden="true" id="' + deleteItemId + '"></i></td>\
				</tr>';
		}
		modalTable += '<tr><td></td><td></td><td>Total: ' + table.total  + '</td><td></td></tr>';
		modalTable += '</tbody></table>';
		populateTables(tables, parseInt(currentTableIndex, 10));
		$('#tableModalBody').html(modalTable);
	}

	$("body").on('click', '.table-body', function(e) {
		let tableIndex = $(this).attr('id').split('table_')[1];
		$("#tableModalCenter").data("table-id", tableIndex);
		refreshModal(this, tableIndex);
	});

	$("body").on('click', '.fa-trash', function(e) {
		const fromTableIndex = $(this).attr('id').split('t-')[1];
		let tableIndex = fromTableIndex.split('-')[0];
		let itemIndex = fromTableIndex.split('-')[2];
		let table = tables[tableIndex];
		table.items.splice(itemIndex, 1);
		refreshModal(this, tableIndex);
	});

	$("body").on('change', '.item-quantity',  function() {
		const fromTableIndex = $(this).attr('id').split('t-')[1];
		let tableIndex = fromTableIndex.split('-')[0];
		let itemIndex = fromTableIndex.split('-')[2];
		let table = tables[tableIndex];
		modifyTableItem(table.items[itemIndex], $(this).val(), table, false);
		refreshModal(this, tableIndex);
	});

	function modifyTableItem(item, quantity, table, isAdd) {
		let currentItem = constructTableItem(item);
		let itemFoundFlag = false;
		for (let i = 0; i < table.items.length; i++) {
			let item = table.items[i];
			if (currentItem.itemName === item.itemName) {
				if(isAdd)
					item.quantity += quantity;
				else 
					item.quantity = quantity;
				itemFoundFlag = true;
			}
		}
		if (!itemFoundFlag)
			table.items.push(currentItem);
		calculateTableTotal(table);
	}

	function calculateTableTotal(table) {
		table.total = 0;
		for (let i = 0; i < table.items.length; i++) {
			let item = table.items[i];
			let itemPrice = item.price * item.quantity;
			table.total += itemPrice;
		}
	}

	function searchRefresh() {
		let tableSearch = $("#table-search").val();
		filterTables(tableSearch, tables);
		let itemSearch = $("#item-search").val();
		filterItems(itemSearch, items);
	}

	function constructTableItem(item) {
		let tableItem = {
			itemName : item.itemName,
			price: item.price,
			courseType: item.courseType,
			quantity: 1
		}
		return tableItem;
	}

	function initializeTables() {
		$.getJSON("http://localhost:3000/tables", function(data) {
			populateTables(data);
		});
	}



	function populateItems(itemsData) {
		$("#items").empty();
		items= itemsData;
		for(var i=0;i<items.length;i++) {
			let item = items[i];
			let uiItem = $('<div class="card border">\
					<div class="card-body">\
							<b>' + item.itemName + '</b><br/>'
							+ item.price + 
					'</div></div>');
			uiItem.data("item-index", i);
			uiItem.draggable();
			$("#items").append(uiItem);
		}
	}

	function initializeItems() {
		$.getJSON("http://localhost:3000/items", function(data) {
			populateItems(data);
		});
	}


	let tables = [];
	initializeTables();
	

	let items = [];
	initializeItems();
	

	
	function filterWithProps(value, tables, props) {
		if(!value) {
			newTables = tables;
		} else {
			newTables = [];
			tables.forEach(function(eachTable) {
				let isMatch = false;
				for(let i=0;i<props.length;i++) {
					let prop = props[i];
					if(eachTable[prop].toLowerCase().indexOf(value) !== -1) {
						isMatch = true;
						break;
					}
				};
				if(isMatch)
					newTables.push(eachTable);
			});
		}
		return newTables;
	}

	function filterItems(value, items) {
		let newItems = filterWithProps(value, items, ["itemName", "courseType"]);
		populateItems(newItems);
	}

	function filterTables(value, tables) {
		let newTables = filterWithProps(value, tables, ["tableName"]);
		populateTables(newTables);
	}
	

	$("#item-search").keyup(function() {
		let value = $("#item-search").val();
		filterItems(value, items);
	});

	$("#table-search").keyup(function() {
		let value = $("#table-search").val();
		filterTables(value, tables);
	});

	$("#popup-close-button").click(function() {
		closePopupAction();
	});

	$("#popup-close-icon").click(function() {
		closePopupAction();
	});
});